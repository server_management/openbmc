inherit systemd
inherit useradd

USERADD_PACKAGES = "${PN}"

# add a user called httpd for the server to assume
USERADD_PARAM:${PN} = "-r -s /usr/sbin/nologin bmcweb"
GROUPADD_PARAM:${PN} = "web; redfish"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=175792518e4ac015ab6696d16c4f607e"

SRC_URI = "gitsm://git.gitlab.arm.com/server_management/bmcweb.git;branch=master;protocol=https;nobranch=1"

PV = "1.0+git${SRCPV}"
SRCREV = "305ea056df3e40e8c4104bd774659828c87ee6e3"

S = "${WORKDIR}/git"

inherit pkgconfig meson ptest

SRC_URI += " \
    file://run-ptest \
"

DEPENDS = " \
    openssl \
    zlib \
    boost \
    boost-url \
    libpam \
    sdbusplus \
    gtest \
    nlohmann-json \
    libtinyxml2 \
    cmake-native \
    ${@bb.utils.contains('PTEST_ENABLED', '1', 'gtest', '', d)} \
    ${@bb.utils.contains('PTEST_ENABLED', '1', 'gmock', '', d)} \
"

RDEPENDS:${PN} += " \
    jsnbd \
    phosphor-mapper \
"

INSANE_SKIP:${PN} = "file-rdeps"

do_install_ptest() {
        install -d ${D}${PTEST_PATH}/test
        cp -rf ${B}/*_test ${D}${PTEST_PATH}/test/
}

FILES:${PN} += "${datadir}/** "


EXTRA_OEMESON = " \
    --buildtype=minsize \
    -Dtests=${@bb.utils.contains('PTEST_ENABLED', '1', 'enabled', 'disabled', d)} \
    -Dyocto-deps=enabled \
"

SYSTEMD_SERVICE:${PN} += "bmcweb.service bmcweb.socket"

FULL_OPTIMIZATION = "-Os "

# Install pldmSoftPowerOff.service in correct targets
pkg_postinst:${PN} () {

    # Install dependent libraries.
    install -d $D/${libdir}
    install -m 0755 ${S}/libcper/lib/libb64c.so $D/${libdir}
    install -m 0755 ${S}/libcper/lib/libjson-c.so $D/${libdir}
    install -m 0755 ${S}/libcper/lib/libjson-c.so.5 $D/${libdir}
}
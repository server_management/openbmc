SUMMARY = "PLDM Stack"
DESCRIPTION = "Implementation of the PLDM specifications"
PR = "r1"
PV = "1.0+git${SRCPV}"

inherit meson pkgconfig
inherit systemd

require pldm.inc

DEPENDS += "function2"
DEPENDS += "systemd"
DEPENDS += "sdeventplus"
DEPENDS += "phosphor-dbus-interfaces"
DEPENDS += "phosphor-logging"
DEPENDS += "nlohmann-json"
DEPENDS += "cli11"
DEPENDS += "cmake-native"
INSANE_SKIP:${PN} = "file-rdeps"

S = "${WORKDIR}/git"

SYSTEMD_SERVICE:${PN} += "pldmd.service"
SYSTEMD_SERVICE:${PN} += "pldmSoftPowerOff.service"

EXTRA_OEMESON = " \
        -Dtests=disabled \
        -Doem-ibm=disabled \
        "

# Install pldmSoftPowerOff.service in correct targets
pkg_postinst:${PN} () {

    # Install dependent libraries.
    install -d $D/${libdir}
    install -m 0755 ${S}/libcper/lib/libb64c.so $D/${libdir}
    install -m 0755 ${S}/libcper/lib/libjson-c.so $D/${libdir}
    install -m 0755 ${S}/libcper/lib/libjson-c.so.5 $D/${libdir}

    mkdir -p $D$systemd_system_unitdir/obmc-host-shutdown@0.target.wants
    LINK="$D$systemd_system_unitdir/obmc-host-shutdown@0.target.wants/pldmSoftPowerOff.service"
    TARGET="../pldmSoftPowerOff.service"
    ln -s $TARGET $LINK

    mkdir -p $D$systemd_system_unitdir/obmc-host-warm-reboot@0.target.wants
    LINK="$D$systemd_system_unitdir/obmc-host-warm-reboot@0.target.wants/pldmSoftPowerOff.service"
    TARGET="../pldmSoftPowerOff.service"
    ln -s $TARGET $LINK
}

pkg_prerm:${PN} () {

    LINK="$D$systemd_system_unitdir/obmc-host-shutdown@0.target.wants/pldmSoftPowerOff.service"
    rm $LINK

    LINK="$D$systemd_system_unitdir/obmc-host-warm-reboot@0.target.wants/pldmSoftPowerOff.service"
    rm $LINK
}

SRC_URI:append = " file://static_eid_table.json "
do_install:append() {
   install -D -m 0644 ${WORKDIR}/static_eid_table.json ${D}/usr/share/pldm
}